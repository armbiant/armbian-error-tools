#####################################
Welcome to |project|'s documentation!
#####################################

.. toctree::
   :maxdepth: 2
   :caption: Contents

   api
   changelog

.. only:: html

   ##################
   Indices and tables
   ##################
   
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
